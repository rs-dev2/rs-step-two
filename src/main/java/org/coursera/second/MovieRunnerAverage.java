package org.coursera.second;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.second.rating.SecondRatings;

import java.util.Collections;
import java.util.List;

/**
 * Hello world!
 */
public class MovieRunnerAverage {
    private static final Logger logger = LogManager.getLogger();

    public void printAverageRatings() {
        SecondRatings sr = new SecondRatings("data/ratedmovies_short.csv", "data/ratings_short.csv");//do i need put filename here?
        logger.info("Movie size = {}", sr.getMovieSize());
        logger.info("Rater size = {}", sr.getRaterSize());
        List<Rating> ratingList = sr.getAverageRatings(2);
        Collections.sort(ratingList);
        for (Rating rating : ratingList) {
            String s = String.format("%-10.2f%s", rating.getValue(), sr.getTitle(rating.getItem()));
            logger.info(s);
        }
    }

    public void getAverageRatingOneMovie() {
        SecondRatings sr = new SecondRatings("data/ratedmovies_short.csv", "data/ratings_short.csv");//do i need put filename here?
        List<Rating> ratingList = sr.getAverageRatings(2);
        String movieTitle = "The Godfather";
        for (Rating rating : ratingList) {
            if (sr.getTitle(rating.getItem()).equals(movieTitle)) {
                String s = String.format("%-10.2f%s", rating.getValue(), sr.getTitle(rating.getItem()));
                logger.info(s);
            }
        }

    }

    public static void main(String[] args) {
        MovieRunnerAverage mra = new MovieRunnerAverage();
        logger.info("---------------print test----------------");
        mra.printAverageRatings();
        logger.info("---------------get average rating one movie test----------------");
        mra.getAverageRatingOneMovie();
    }
}
